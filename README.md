### Infrastructure as a code

Infrastructure as Code is one of the principles of DevOps

IaC helps to accelerate the deployment of environments for different purposes and provides high performance

As Patrick Debois said
> High performance is achieved due to controllability and the ability to deploy quickly

##### Why IaC?

- The only point of truth
- Idempotence
- Price
- Speed
- Risk reduction

Tools needed for the project

1. [GCP](https://cloud.google.com/)
2. [Slack](https://slack.com/intl/en-ua/)
3. [Git](https://git-scm.com/)
4. [Packer](https://www.packer.io/)
5. [Terraform](https://www.terraform.io/)
6. [Ansible](https://www.ansible.com/)
7. [Docker](https://www.docker.com/)
8. [Gitlab CI](https://docs.gitlab.com/ee/ci/)
9. [Prometheus](https://prometheus.io/)
10. [Grafana](https://grafana.com/)

Tool | Appointment
------------ | -------------
GCP | Cloud platform
Slack | Messenger for communication and notification
Git | To store the project code
Packer | Collection of images of virtual machines
Terraform | Infrastructure management
Ansible | Configuration management
Docker | Container assembly and management
Gitlab CI | Continuous integration
Prometheus | Monitoring of events and notifications
Grafana | Visualization

##### Usage

`Git`

```
mkdir <project_name>
git clone <link_on_gitlab_project>

```

`Terraform`

```
cd infra / terraform
terraform init
terraform plan
terraform apply

```

At the output we get in the output IP intance, after which you can check that the application works 
